package com.binovsky.RemoteBot;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.RemoteViews;

public class RemoteBotWidget extends AppWidgetProvider
{
	@Override
	public void onEnabled( Context context )
	{
		Log.w( "RemoteBotWidget", "onEnable" );

		RemoteViews remoteViews = new RemoteViews( context.getPackageName(),
				R.layout.widget );
		remoteViews.setTextViewText( R.id.widgetToggleButton,
				String.valueOf( R.string.server_start ) );

		// Register an onClickListener
		Intent intent = new Intent( context, RemoteBotWidget.class );

		intent.setAction( AppWidgetManager.ACTION_APPWIDGET_UPDATE );
		intent.putExtra( AppWidgetManager.EXTRA_APPWIDGET_IDS,
				R.id.widgetToggleButton );
	}

	@Override
	public void onUpdate( Context context, AppWidgetManager appWidgetManager,
			int[] appWidgetIds )
	{
		Log.w( "RemoteBotWidget", "onUpdate" );
	}
}
