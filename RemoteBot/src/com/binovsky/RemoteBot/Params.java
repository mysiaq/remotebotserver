package com.binovsky.RemoteBot;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention ( RetentionPolicy.RUNTIME)
public @interface Params
{
	Class[] types();

	String[] names();
}
