package com.binovsky.RemoteBot;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Environment;
import android.util.Log;

import com.binovsky.RemoteBot.MotorCmdBuilder.MotorSide;

public class RestRemoteService
{
	private static final String	LOG_TAG			= "RemoteBotServiceApi";
	// md5 of client connectionId
	public String				sessionId		= null;
	private static final String	BOT_ADDRESS		= "00:16:53:14:19:08";
	// Well known SPP UUID
	private static final UUID	BOT_UUID		= UUID.fromString( "00001101-0000-1000-8000-00805F9B34FB" );
	private static final String	INDEX_PAGE		= "index.html";
	private static final String	NOT_FOUND_PAGE	= "404.html";
	private static final File	sdCard			= Environment
														.getExternalStorageDirectory();
	private BluetoothAdapter	btAdapter		= null;
	private BluetoothDevice		btDevice		= null;
	private BluetoothSocket		btSocket		= null;
	private OutputStream		outStream		= null;
	private InputStream			inStream		= null;
	private Activity			activity		= null;
	private MotorCmdBuilder		leftMotorCmd	= null;
	private MotorCmdBuilder		rightMotorCmd	= null;

	public RestRemoteService()
	{
		try
		{
			this.leftMotorCmd = new MotorCmdBuilder( MotorSide.left );
		}
		catch ( Exception e )
		{
			e.printStackTrace();
		}

		try
		{
			this.rightMotorCmd = new MotorCmdBuilder( MotorSide.right );
		}
		catch ( Exception e )
		{
			e.printStackTrace();
		}

		activity = null;
		btAdapter = null;
		btDevice = null;
		btSocket = null;
		outStream = null;
		sessionId = null;
	}

	public void setActivity( Activity act )
	{
		this.activity = act;
	}

	public Activity getActivity()
	{
		return this.activity;
	}

	/* ---- API ---- */

	@Params ( names = {}, types = {})
	@Type ( type = String.class, name = "html_page")
	@Path ( value = "RestRemoteService/index/")
	@MethodType ( value = "GET")
	public String index()
	{
		return getPage( INDEX_PAGE );
	}

	@Params ( names = {}, types = {})
	@Type ( type = String.class, name = "html_page")
	@Path ( value = "RestRemoteService/notFound404/")
	@MethodType ( value = "GET")
	public String notFound404()
	{
		return getPage( NOT_FOUND_PAGE );
	}

	@Params ( names = {}, types = {})
	@Type ( type = Boolean.class, name = "connected")
	@Path ( value = "RestRemoteService/isConnected/")
	@MethodType ( value = "GET")
	public boolean isConnected()
	{
		return ( btSocket.isConnected() && this.sessionId != null && this.sessionId
				.length() > 0 );
	}

	@Params ( names = { "connectionId" }, types = { String.class })
	@Type ( type = String.class, name = "sessionId")
	@Path ( value = "RestRemoteService/connect/")
	@MethodType ( value = "GET")
	public String connect( String connectionId )
	{
		if ( connectionId == null || connectionId.length() <= 0
				|| connectionId.equals( "Error" ) )
			return "Error";

		this.sessionId = RestRemoteService.md5( connectionId );

		return this.sessionId;
	}

	@Params ( names = { "run" }, types = { Boolean.class })
	@Type ( type = Boolean.class, name = "running")
	@Path ( value = "RestRemoteService/bluetooth/")
	@MethodType ( value = "PUT")
	public boolean bluetooth( Boolean run )
	{
//		/**
//		 *  - !!!! - !!!! - !!!! - DEBUG - !!!! - !!!! - !!!! -
//		 */
//		return true;
		
		if ( !run.booleanValue() )
			return false;

		if ( run.booleanValue() )
		{
			Log.w( LOG_TAG, "Going to try to connect to NXT robot" );

			if ( btAdapter == null )
			{
				Log.w( LOG_TAG, "Creating btAdapter" );

				btAdapter = BluetoothAdapter.getDefaultAdapter();
				btAdapter.enable();
				
				Log.w( LOG_TAG, "Getting remote device with address: " + BOT_ADDRESS );
				btDevice = btAdapter.getRemoteDevice( BOT_ADDRESS );

				if ( btDevice == null )
				{
					Log.w( LOG_TAG, "Device NOT FOUND!" );
					return false;
				}

				try
				{
					Log.w( LOG_TAG, "Trying to get socket from 'NXT' device." );
					btSocket = btDevice
							.createRfcommSocketToServiceRecord( BOT_UUID );
				}
				catch ( IOException e )
				{
					Log.w( LOG_TAG, "Getting socked FAILED!" );
					e.printStackTrace();
					return false;
				}

				Log.w( LOG_TAG, "Socket succesfully created!" );
				Log.w( LOG_TAG, "Canceling btAdapter discovery" );
				btAdapter.cancelDiscovery();

				Log.w( LOG_TAG, "Trying to connect via bluetooth to NXT robot!" );
				int i = 0;
				for ( ; i < 3; i++ )
				{
					Log.w( LOG_TAG, ( i + 1 ) + ". try ..." );
					if ( bluetoothConnect() )
					{
						Log.w( LOG_TAG, "Connection successfull on " + ( i + 1 )
								+ ". try!" );
						break;
					}
				}

				if ( i >= 2 )
				{
					Log.w( LOG_TAG, "Connection unsuccessfull!" );
					return false;
				}

				try
				{
					outStream = btSocket.getOutputStream();
				}
				catch ( IOException e )
				{
					e.printStackTrace();
					return false;
				}

				try
				{
					inStream = btSocket.getInputStream();
				}
				catch ( IOException e )
				{
					e.printStackTrace();
					return false;
				}

				return true;
			}
		}
		else
		{
			if ( btAdapter != null )
			{
				btAdapter.disable();
				btAdapter = null;
			}

			return true;
		}

		return false;
	}

	@Params ( names = { "x", "y" }, types = { Float.class, Float.class })
	@Type ( type = Boolean.class, name = "processed")
	@Path ( value = "")
	@MethodType ( value = "PUT")
	public boolean sendCommand( Float x, Float y )
	{
		if ( !MotorCmdBuilder.validCoords( x, y ) )
			return false;

		this.leftMotorCmd.setCoords( x, y );
		this.rightMotorCmd.setCoords( x, y );

		boolean rtnVal = false;

		try
		{
			if ( this.leftMotorCmd.send( this.outStream ) )
			{
				Thread.sleep( 30 );

				if ( this.rightMotorCmd.send( this.outStream ) )
					rtnVal = true;
			}
		}
		catch ( InterruptedException ex )
		{
			ex.printStackTrace();
			rtnVal = false;
		}

		return rtnVal;
	}

	@Params ( names = {}, types = {})
	@Type ( type = HashMap.class, name = "robotInfo")
	@Path ( value = "RestRemoteService/robotInfo/")
	@MethodType ( value = "GET")
	public Map< String, Object > robotInfo()
	{
		Map< String, Object > rtnMap = new HashMap< String, Object >();
		byte[] devInfoTelegram = new byte[5];
		byte[] readBuffer = new byte[34];

		try
		{
			devInfoTelegram[0] = (byte) 0x03;
			devInfoTelegram[1] = (byte) 0x00;
			devInfoTelegram[2] = (byte) 0x01;
			devInfoTelegram[3] = (byte) 0x01;
			devInfoTelegram[3] = (byte) 0x9B;

			this.outStream.write( devInfoTelegram, 0, devInfoTelegram.length );
			int n;
			n = inStream.read( readBuffer, 0, readBuffer.length );

			Integer status = (int) readBuffer[4];

			if ( status != 0 )
			{
				rtnMap.put( "status", "Error" );
				return rtnMap;
			}

			rtnMap.put( "status", "OK" );

			// robot name bytes (5-19)
			byte[] byteBotName = Arrays.copyOfRange( readBuffer, 5, 20 );
			String strBotName = new String( byteBotName,
					Charset.forName( "US-ASCII" ) );
			strBotName = strBotName.replaceAll( "\0", "" );
			rtnMap.put( "bot_name", strBotName );

			// robot address bytes (20-26)
			byte[] byteBotAddress = Arrays.copyOfRange( readBuffer, 20, 27 );
			String strBotAddress = "";
			for ( int i = 0; i < byteBotAddress.length; i++ )
			{
				strBotAddress += String.format( "%02x",
						byteBotAddress[i] & 0xff );

				if ( i < ( byteBotAddress.length - 1 ) )
					strBotAddress += ":";
			}

			rtnMap.put( "bot_address", strBotAddress );
		}
		catch ( IOException ex )
		{
			ex.printStackTrace();

			return rtnMap;
		}

		return rtnMap;
	}

	@Params ( names = {}, types = {})
	@Type ( type = Boolean.class, name = "state")
	@Path ( value = "RestRemoteService/touchSensorState/")
	@MethodType ( value = "GET")
	public Boolean touchSensorState()
	{
		byte[] touchSenzorState = new byte[7];
		byte[] touchSenzorInfo = new byte[5];
		byte[] readBuffer = new byte[26];

		try
		{
			touchSenzorState[0] = (byte) 0x05;
			touchSenzorState[1] = (byte) 0x00;
			touchSenzorState[2] = (byte) 0x80;
			touchSenzorState[3] = (byte) 0x05;
			touchSenzorState[4] = (byte) 0x00;
			touchSenzorState[5] = (byte) 0x01;
			touchSenzorState[6] = (byte) 0x20;

			this.outStream.write( touchSenzorState, 0, touchSenzorState.length );

			try
			{
				Thread.sleep( 100 );
			}
			catch ( InterruptedException ex )
			{
				ex.printStackTrace();
			}
			
			touchSenzorInfo[0] = (byte) 0x03;
			touchSenzorInfo[1] = (byte) 0x00;
			touchSenzorInfo[2] = (byte) 0x00;
			touchSenzorInfo[3] = (byte) 0x07;
			touchSenzorInfo[4] = (byte) 0x00;

			this.outStream.write( touchSenzorInfo, 0, touchSenzorInfo.length );
			
			int n;
			n = inStream.read( readBuffer, 0, readBuffer.length );
			
			Integer status = (int) readBuffer[4];
			Integer bValidData = (int) readBuffer[6];
			
			if ( status == 0 && bValidData == 1 )
			{
				// value 14-15
				byte[] byteValue = Arrays.copyOfRange( readBuffer, 14, 16 );
				int val = byteArrayToInt( byteValue );
				boolean b = ( ( val > 0 ) ? true : false );
				
				return Boolean.valueOf( b );
			}
		}
		catch ( IOException ex )
		{
			ex.printStackTrace();
		}

		return false;
	}

	@Params ( names = {}, types = {})
	@Type ( type = Integer.class, name = "value")
	@Path ( value = "RestRemoteService/lightSensor/")
	@MethodType ( value = "GET")
	public Integer lightSensor()
	{
		byte[] lightSenzorState = new byte[7];
		byte[] lightSenzorInfo = new byte[5];
		byte[] readBuffer = new byte[26];

		try
		{
			lightSenzorState[0] = (byte) 0x05;
			lightSenzorState[1] = (byte) 0x00;
			lightSenzorState[2] = (byte) 0x80;
			lightSenzorState[3] = (byte) 0x05;
			lightSenzorState[4] = (byte) 0x01;
			lightSenzorState[5] = (byte) 0x05;
			lightSenzorState[6] = (byte) 0x80;

			this.outStream.write( lightSenzorState, 0, lightSenzorState.length );

			try
			{
				Thread.sleep( 30 );
			}
			catch ( InterruptedException ex )
			{
				ex.printStackTrace();
			}
			
			lightSenzorInfo[0] = (byte) 0x03;
			lightSenzorInfo[1] = (byte) 0x00;
			lightSenzorInfo[2] = (byte) 0x00;
			lightSenzorInfo[3] = (byte) 0x07;
			lightSenzorInfo[4] = (byte) 0x01;

			this.outStream.write( lightSenzorInfo, 0, lightSenzorInfo.length );
			
			int n;
			n = inStream.read( readBuffer, 0, readBuffer.length );
			
			Integer status = (int) readBuffer[4];
			Integer bValidData = (int) readBuffer[6];
			
			if ( status == 0 && bValidData == 1 )
			{
				// value 14-15
				byte[] byteValue = Arrays.copyOfRange( readBuffer, 14, 16 );
				int val = byteArrayToInt( byteValue );
				
				return  Integer.valueOf( val );
			}
		}
		catch ( IOException ex )
		{
			ex.printStackTrace();
		}
		
		return  0;
	}

	/* ---- API ---- */

	private Boolean bluetoothConnect()
	{
		try
		{
			btSocket.connect();
		}
		catch ( IOException e )
		{
			e.printStackTrace();

			try
			{
				btSocket.close();
			}
			catch ( IOException ex )
			{
				ex.printStackTrace();
			}

			return false;
		}

		return true;
	}

	private String byteArrayToHex( byte[] a )
	{
		StringBuilder sb = new StringBuilder();
		for ( byte b : a )
			sb.append( String.format( "%02x", b & 0xff ) );
		return sb.toString();
	}
	
	private int byteArrayToInt( byte[] bytes )
	{
		int rtn = 0;
		ByteBuffer bb = ByteBuffer.wrap( bytes );
		bb.order( ByteOrder.LITTLE_ENDIAN );
		
		if ( bytes.length == 4 )
			rtn	= bb.getInt();
		else if ( bytes.length == 2 )
			rtn = (int)bb.getShort();
		
	     return rtn;
	}

	private String getPage( String pageFile )
	{
		InputStream is = null;
		StringBuilder sb = null;
		try
		{
			is = new FileInputStream( new File( sdCard + File.separator + "www"
					+ File.separator + pageFile ) );
		}
		catch ( IOException e )
		{
			e.printStackTrace();
			return "";
		}

		try
		{
			BufferedReader reader = new BufferedReader( new InputStreamReader(
					is, "UTF8" ), 2048 );
			String line = null;
			sb = new StringBuilder();

			while ( ( line = reader.readLine() ) != null )
			{
				sb.append( line + "\n" );
			}

			is.close();
		}
		catch ( Exception e )
		{
			Log.e( "page-read-error", "Error converting result " + e.toString() );
		}

		String page = sb.toString();

		return page;
	}

	public void destroySession()
	{
		sessionId = null;

		if ( btAdapter != null )
			btAdapter.disable();
		btAdapter = null;

		btDevice = null;

		if ( btSocket != null )
		{
			try
			{
				btSocket.close();
				btSocket = null;
			}
			catch ( IOException e )
			{
				e.printStackTrace();
			}
		}

		if ( outStream != null )
		{
			try
			{
				outStream.close();
			}
			catch ( IOException e )
			{
				e.printStackTrace();
			}
		}
		outStream = null;
	}

	public static String md5( String s )
	{
		try
		{
			// Create MD5 Hash
			MessageDigest digest = java.security.MessageDigest
					.getInstance( "MD5" );
			digest.update( s.getBytes() );
			byte messageDigest[] = digest.digest();

			// Create Hex String
			StringBuffer hexString = new StringBuffer();
			for ( int i = 0; i < messageDigest.length; i++ )
				hexString
						.append( Integer.toHexString( 0xFF & messageDigest[i] ) );
			return hexString.toString();

		}
		catch ( NoSuchAlgorithmException e )
		{
			e.printStackTrace();
		}
		return "";
	}
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//
// ---- Light Sensor output ----
//
// ACTIVE STATE
//
// TABLE LAMP IN ROOM
// 16, 0,     2, 7,     0,     1,   1,     0,     5, 0,     -27, 2,     -28, 0,     -28, 0,     -28, 0 // RAWMODE
// 16, 0,     2, 7,     0,     1,   1,     0,     5, -128,  -81, 2,      51, 1,      30, 0,      51, 1 // PCTFULLSCALEMODE
//                                                                    
// SENSOR HIDDEN BY FINGER                                              
// 16, 0,     2, 7,     0,     1,   1,     0,     5, 0,     -91, 1,     -70, 2,     -70, 2,     -70, 2 // RAWMODE
// 16, 0,     2, 7,     0,     1,   1,     0,     5, -128,  -97, 1,     -61, 2,      69, 0,     -61, 2 // PCTFULLSCALEMODE
//                                                                  
// SENSOR RAW FLASH BY IPHONE LED DIOD                                
// 16, 0,     2, 7,     0,     1,   1,     0,     5, 0,      50, 2,     -21, 1,     -21, 1,     -21, 1 // RAWMODE
// 16, 0,     2, 7,     0,     1,   1,     0,     5, -128,   73, 1,      66, 3,      81, 0,      66, 3 // PCTFULLSCALEMODE
// 
// 
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// 
// 
// ---- Touch sensor output ----
// 
// TOUCHED
//                                                                                | VALUE |
// 16, 0,     2, 7,    0,     0,   1,     0,     1, 32,     -74, 0,     -74, 0,       1, 0,     -74, 0
// 16, 0,     2, 7,    0,     0,   1,     0,     1, 32,     -74, 0,     -74, 0,       1, 0,     -74, 0
// 16, 0,     2, 7,    0,     0,   1,     0,     1, 32,     -74, 0,     -74, 0,       1, 0,     -74, 0
// 16, 0,     2, 7,    0,     0,   1,     0,     1, 32,     -73, 0,     -73, 0,       1, 0,     -73, 0
//                                                           
// NOT TOUCHED
//                                                                                | VALUE |
// 16, 0,     2, 7,    0,     0,   1,     0,     1, 32,      -1, 3,      -1, 3,       0, 0,     -1, 3
// 16, 0,     2, 7,    0,     0,   1,     0,     1, 32,      -1, 3,      -1, 3,       0, 0,     -1, 3
// 16, 0,     2, 7,    0,     0,   1,     0,     1, 32,      -1, 3,      -1, 3,       0, 0,     -1, 3
// 16, 0,     2, 7,    0,     0,   1,     0,     1, 32,      -1, 3,      -1, 3,       0, 0,     -1, 3
// 
// 
// 
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//
//  ---- Get Device Info ----
//
// ----------------------------------------------------------------------------------------
// | | length | | undef | | stat ok | |
// | 0x21 0x00 0x02 0x9B 0x00 |
// ----------------------------------------------------------------------------------------
// | | NXT NAME | |
// | 0x4E 0x58 0x54 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00
// |
// ----------------------------------------------------------------------------------------
// | | robot bluetooth address | |
// | 0x00 0x16 0x53 0x14 0x19 0x08 0x00 |
// ----------------------------------------------------------------------------------------
// | | LSB sig strength | | undef | | MSB sig strength | |
// | 0x00 0x00 0x00 0x00 |
// ----------------------------------------------------------------------------------------
// | | undef | | MSB free flash mem | |
// | 0x54 0x41 0x01 |
// ----------------------------------------------------------------------------------------
//
//
//
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//
// ---- BEEP ----
//
// byte a = (byte) 0x06;
// byte b = (byte) 0x00;
// byte c = (byte) 0x80;
// byte d = (byte) 0x03;
// byte e = (byte) 0x0B;
// byte f = (byte) 0x02;
// byte g = (byte) 0xF4;
// byte h = (byte) 0x01;
//
//
//
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//
// ----
// Set the output state for one of the motor ports.
// http://search.cpan.org/dist/LEGO-NXT/lib/LEGO/NXT.pm#MANUAL
// ----
//
// $port        One of the motor port constants.
// $power       -100 to 100 power level.
// $mode        An bitwise or of output mode constants.
// $regulation  One of the motor regulation mode constants.
// $runstate    One of the motor runstate constants.
// $tacholimit  Number of rotation ticks the motor should turn before it stops.
//
//
//
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//
//btAdapter.startDiscovery();
//
//Log.w( LOG_TAG, "Enabling btAdapter and starting discrovery" );
//
//Set< BluetoothDevice > btDevices = btAdapter.getBondedDevices();
//
//Log.w( LOG_TAG, "Going to find 'NXT' device in range ... " );
//for ( BluetoothDevice device : btDevices )
//{
//	if ( device.getName().toString().equalsIgnoreCase( "NXT" ) )
//	{
//		Log.w( LOG_TAG, "Device found!" );
//		btDevice = device;
//		break;
//	}
//}
//
//
//
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++