package com.binovsky.RemoteBot;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity
{
	private static final String	SERVER_STATE	= "serverState";
	private static final String	CONSOLE_TEXT	= "consoleText";
	private static final String	WWW_ROOT		= "www";

	public static final String	SUFFIX_HTML		= ".html", SUFFIX_CSS = ".css",
			SUFFIX_PNG = ".png", SUFFIX_JPG = ".jpg", SUFFIX_JPEG = ".jpeg",
			SUFFIX_GIF = ".gif", SUFFIX_XML = ".xml", SUFFIX_JSON = ".json";

	public enum ServerState
	{
		Stopped( 0 ), Running( 1 );

		private int	code;

		private ServerState( int c )
		{
			code = c;
		}

		public int getCode()
		{
			return code;
		}
	};

	public TextView					console		= null;
	private Button					srvButton	= null;
	private static RemoteBotHTTPD	server		= null;
	private ServerState				serverState	= ServerState.Stopped;

	@Override
	public void onCreate( Bundle savedInstanceState )
	{
		super.onCreate( savedInstanceState );

		setContentView( R.layout.activity_main );
		console = (TextView) findViewById( R.id.server_console );
		srvButton = (Button) findViewById( R.id.server_button );

		if ( savedInstanceState != null )
		{
			int srvState = savedInstanceState.getInt( SERVER_STATE );
			if ( ServerState.Running.getCode() == srvState )
				serverState = ServerState.Running;
			else
				serverState = ServerState.Stopped;

			console.setText( savedInstanceState.getString( CONSOLE_TEXT ) );
		}

		setServerButtonText();
	}

	@Override
	public boolean onCreateOptionsMenu( Menu menu )
	{
		// getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	@Override
	protected void onResume()
	{
		super.onResume();
	}

	@Override
	public void onSaveInstanceState( Bundle savedInstanceState )
	{
		savedInstanceState.putInt( SERVER_STATE, serverState.getCode() );
		savedInstanceState.putString( CONSOLE_TEXT, console.getText()
				.toString() );

		super.onSaveInstanceState( savedInstanceState );
	}

	public void onClickServerButton( View v )
	{
		TextView message = new TextView( this );
		message.setTextColor( Color.RED );
		message.setTextSize( 32.f );

		Button serverButton = (Button) v;
		String serverButtonText = serverButton.getText().toString();

		if ( serverButtonText
				.equalsIgnoreCase( getString( R.string.server_start ) ) )
		{

			if ( startServer() )
			{
				message.setText( R.string.server_started );
			}
			else
			{
				message.setText( "" );
			}
		}
		else if ( serverButtonText
				.equalsIgnoreCase( getString( R.string.server_stop ) ) )
		{
			message.setText( R.string.server_stopped );
			stopServer();
		}
		else
		{
			message.setTag( R.string.unknow_error );
		}

		setServerButtonText();
		Toast alert = new Toast( this );
		alert.setView( message );
		alert.show();
	}

	private void setServerButtonText()
	{
		if ( srvButton == null )
			return;

		switch ( serverState )
		{
			case Running:
				srvButton.setText( getString( R.string.server_stop ) );
				break;
			case Stopped:
				srvButton.setText( getString( R.string.server_start ) );
				break;
			default:
				break;
		}
	}

	private boolean startServer()
	{
		TextView textConsole = (TextView) findViewById( R.id.server_console );
		WifiManager wifiManager = (WifiManager) getSystemService( WIFI_SERVICE );
		int ipAddress = wifiManager.getConnectionInfo().getIpAddress();
		final String formatedIpAddress = String.format( "%d.%d.%d.%d",
				( ipAddress & 0xff ), ( ipAddress >> 8 & 0xff ),
				( ipAddress >> 16 & 0xff ), ( ipAddress >> 24 & 0xff ) );
		textConsole.setText( "Please access! http://" + formatedIpAddress + ":"
				+ RemoteBotHTTPD.PORT );

		if ( server == null )
		{
			if ( isNetworkAvailable() )
			{
				try
				{
					File sdCard = Environment.getExternalStorageDirectory();
					String wwwRootPath = sdCard.getAbsolutePath()
							+ File.separator + WWW_ROOT;
					File wwwRoot = new File( wwwRootPath );

					if ( !wwwRoot.exists() )
					{
						wwwRoot.mkdir();
						copyAssets();
					}

					server = new RemoteBotHTTPD( wwwRoot );
					server.setActivity( this );
					serverState = ServerState.Running;
				}
				catch ( IOException e )
				{
					e.printStackTrace();
					serverState = ServerState.Stopped;

					return false;
				}

				return true;
			}
			else
			{
				Builder builder = new AlertDialog.Builder( this );
				builder.setMessage( R.string.no_network );
				builder.setCancelable( true );
				builder.setNegativeButton( R.string.ok,
						new CancelOnClickListener() );
				AlertDialog dialog = builder.create();
				dialog.show();

				return false;
			}
		}

		return false;
	}

	private void stopServer()
	{
		if ( server != null )
		{
			server.stop();
			server = null;
			serverState = ServerState.Stopped;
		}
	}

	private void copyAssets()
	{
		AssetManager assetManager = getAssets();
		ArrayList< String > files = null;
		File sdCard = Environment.getExternalStorageDirectory();

		files = listAssetDir( assetManager, WWW_ROOT );

		int i = files.size() - 1;
		String filename = "";
		for ( ; i >= 0; i-- )
		{

			InputStream in = null;
			OutputStream out = null;
			try
			{
				filename = files.get( i );
				in = assetManager.open( filename );

				String strSdCardPath = sdCard.getAbsolutePath()
						+ File.separator + filename;

				if ( containsFileSuffix( strSdCardPath ) )
				{
					String[] explodedPath = strSdCardPath
							.split( File.separator );
					StringBuilder strBuilder = new StringBuilder();
					String strDirPath = "";

					for ( int j = 0; j < explodedPath.length - 1; j++ )
					{
						strBuilder.append( explodedPath[j] + File.separator );
					}

					strDirPath = strBuilder.toString();

					File dir = new File( strDirPath );
					if ( !dir.exists() )
						dir.mkdirs();
				}

				File f = new File( strSdCardPath );
				out = new FileOutputStream( f );

				copyFile( in, out );

				in.close();
				in = null;
				out.flush();
				out.close();
				out = null;
			}
			catch ( IOException e )
			{
				Log.e( "www-root", "Failed to copy asset file: " + filename, e );
			}
		}
	}

	private void copyFile( InputStream in, OutputStream out )
			throws IOException
	{
		byte[] buffer = new byte[2048];
		int read;
		while ( ( read = in.read( buffer ) ) != -1 )
		{
			out.write( buffer, 0, read );
		}
	}

	public static ArrayList< String > listAssetDir( AssetManager am,
			String strDir )
	{
		try
		{
			String[] arr = am.list( strDir );
			if ( arr != null && arr.length > 0 )
			{
				ArrayList< String > arrList = new ArrayList< String >();

				for ( String string : arr )
				{
					string = strDir + "/" + string;
					arrList.add( string );
					ArrayList< String > arrs = listAssetDir( am, string );
					if ( arrs != null )
						arrList.addAll( arrs );
				}

				return arrList;
			}
		}
		catch ( IOException e )
		{
			e.printStackTrace();
		}

		return null;
	}

	public static boolean containsFileSuffix( String strToCheck )
	{
		return ( strToCheck.endsWith( SUFFIX_CSS )
				|| strToCheck.endsWith( SUFFIX_GIF )
				|| strToCheck.endsWith( SUFFIX_HTML )
				|| strToCheck.endsWith( SUFFIX_JPEG )
				|| strToCheck.endsWith( SUFFIX_JPG )
				|| strToCheck.endsWith( SUFFIX_JSON )
				|| strToCheck.endsWith( SUFFIX_PNG ) || strToCheck
					.endsWith( SUFFIX_XML ) );
	}

	public boolean isNetworkAvailable()
	{
		ConnectivityManager cm = (ConnectivityManager) getSystemService( Context.CONNECTIVITY_SERVICE );
		NetworkInfo networkInfo = cm.getActiveNetworkInfo();

		if ( networkInfo != null && networkInfo.isConnected() )
		{
			return true;
		}
		return false;
	}

	private final class CancelOnClickListener implements
			DialogInterface.OnClickListener
	{
		public void onClick( DialogInterface dialog, int which )
		{
			Toast.makeText( getApplicationContext(), R.string.check_connection,
					Toast.LENGTH_LONG ).show();
		}
	}

	public void alertBox( String title, String message )
	{
		new AlertDialog.Builder( this ).setTitle( title )
				.setMessage( message + R.string.press_ok_to_cancel )
				.setPositiveButton( R.string.ok, new OnClickListener()
				{
					public void onClick( DialogInterface arg0, int arg1 )
					{
						finish();
					}
				} ).show();
	}
}