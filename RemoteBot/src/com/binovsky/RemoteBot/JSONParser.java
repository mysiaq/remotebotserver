package com.binovsky.RemoteBot;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.json.JSONException;
import org.json.JSONObject;

import android.hardware.Camera.Size;
import android.util.Log;

public class JSONParser
{
	static InputStream	is		= null;
	static JSONObject	jObj	= null;
	static String		json	= "";

	// constructor
	public JSONParser()
	{

	}

	public JSONObject parse( String file )
	{
		if ( file == null || file.length() <= 0 )
			return jObj;

		try
		{
			is = new FileInputStream( file );

		}
		catch ( FileNotFoundException e )
		{
			e.printStackTrace();
			return null;
		}

		try
		{
			BufferedReader reader = new BufferedReader( new InputStreamReader(
					is, "utf-8" ), 2048 );
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ( ( line = reader.readLine() ) != null )
			{
				sb.append( line + "\n" );
			}
			is.close();
			json = sb.toString();
		}
		catch ( Exception e )
		{
			Log.e( "Buffer Error", "Error converting result " + e.toString() );
		}

		// try parse the string to a JSON object
		try
		{
			jObj = new JSONObject( json );
		}
		catch ( JSONException e )
		{
			Log.e( "JSON Parser", "Error parsing data " + e.toString() );
		}

		// return JSON String
		return jObj;
	}
}
