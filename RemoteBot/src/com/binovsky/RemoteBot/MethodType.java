package com.binovsky.RemoteBot;

public @interface MethodType
{
	String value();
}
