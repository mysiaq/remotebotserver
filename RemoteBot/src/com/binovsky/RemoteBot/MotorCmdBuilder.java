package com.binovsky.RemoteBot;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;

/*
 * Syntax
 *
 *	[0x0C] [0x00] [0x00 or 0x80] [0x04] [0x00-0x02 or 0xFF Motor output port] 
 *	[Motor power set point] [Mode byte] [Regulation mode] [Turn ratio] [Run state] 
 *	[Tacho limit LSB] […] […] [Tach limit MSB]
 *
 *	Explanation:
 *
 *	Byte 0-1: Command length LSB first.
 *	Byte 2: Command type- direct command. For direct command with response message use 0x00, otherwise,
 *			for direct command without the reply message, use 0x80.
 *	Byte 3: Command- set motor output state.
 *	Byte 4: Motor output port.
 *	Byte 5: Motor power set point.
 *	Byte 6: Motor mode byte (bit field).
 *	Byte 7: Regulation mode. It is valid only when the motor mode is regulated, otherwise use 0x00 value.
 *	Byte 8: Turn ratio. It is valid only when using a motors synchronization regulation mode, otherwise use 0x00 value.
 *	Byte 9: Run state. See explanation for Run state below.
 *	Byte 10-13: Tacho limit LSB first. Valid only when using a ramp-up or ramp-down as a Run stae, otherwise use 0x00 value.
 *
 */

public class MotorCmdBuilder
{
	/*
	 * constant command bytes
	 */
	private static final byte	motorCmdLength1					= (byte) 0x0C;
	private static final byte	motorCmdLength2					= (byte) 0x00;
	private static final byte	motorCmdTypeDirect				= (byte) 0x80;
	private static final byte	motorOutputState				= (byte) 0x04;

	/*
	 * motor ports
	 */
	private static final byte	motorPortA						= (byte) 0x00;
	private static final byte	motorPortB						= (byte) 0x01;
	private static final byte	motorPortC						= (byte) 0x02;
	private static final byte	motorPortAll					= (byte) 0xFF;

	/*
	 * motor mode byte
	 */
	private static final byte	modeCoast						= (byte) 0x00;
	private static final byte	modeMotorOn						= (byte) 0x01;
	private static final byte	modeBreak						= (byte) 0x02;	// nothing
																				// will
																				// happen,
																				// do
																				// not
																				// use
	private static final byte	modeMotorOnWithBreak			= (byte) 0x03;
	private static final byte	modeRegulated					= (byte) 0x04;	// nothing
																				// will
																				// happen,
																				// do
																				// not
																				// use
	private static final byte	modeMotorOnRegulated			= (byte) 0x05;
	private static final byte	modeMotorOnBreakAndregulated	= (byte) 0x07;

	/*
	 * regulation mode byte
	 */
	private static final byte	regulationNo					= (byte) 0x00;
	private static final byte	regulationSpeed					= (byte) 0x01;
	private static final byte	regulationSynchronized			= (byte) 0x02;

	/*
	 * turn ration byte
	 */
	private static final byte	turnNo							= (byte) 0x00;

	/*
	 * run state byte
	 */
	private static final byte	runStateIdle					= (byte) 0x00;
	private static final byte	runStateRampUp					= (byte) 0x10;
	private static final byte	runStateRunning					= (byte) 0x20;
	private static final byte	runStateRampDown				= (byte) 0x40;

	public enum MotorSide
	{
		invalid, left, right
	}

	private enum Quadrant
	{
		invalid, first, second, third, fourth
	}

	private static enum CommandBytes
	{
		byteLength1, byteLength2, byteType, byteMotorOutputState, byteMotorOutputPort, byteMotorPower, byteMotorMode, byteRegulationMode, byteTurnRation, byteRunState, byteTachoLimit1, byteTachoLimit2, byteTachoLimit3, byteTachoLimit4
	}

	private Float		axisX		= 0.f;
	private Float		axisY		= 0.f;
	private MotorSide	motorSide	= MotorSide.invalid;
	private Quadrant	quadrant	= Quadrant.invalid;
	private int			power		= 0;
	private String		strHexPower	= "";
	private byte[]		bytePower	= null;
	private byte		motorPort	= (byte) MotorCmdBuilder.motorPortAll;
	private byte[]		command		= new byte[14];

	public MotorCmdBuilder( MotorSide side ) throws Exception
	{
		if ( side == MotorSide.invalid )
			throw new Exception( "Invalid motor side." );

		this.motorSide = side;

		try
		{
			this.motorPort = getDefaultMotorPort();
		}
		catch ( Exception e )
		{
			e.printStackTrace();
		}

		command[CommandBytes.byteLength1.ordinal()] = motorCmdLength1;
		command[CommandBytes.byteLength2.ordinal()] = motorCmdLength2;
		command[CommandBytes.byteType.ordinal()] = motorCmdTypeDirect;
		command[CommandBytes.byteMotorOutputState.ordinal()] = motorOutputState;
		command[CommandBytes.byteMotorOutputPort.ordinal()] = motorPort;
		command[CommandBytes.byteMotorMode.ordinal()] = modeMotorOnRegulated;
		command[CommandBytes.byteRegulationMode.ordinal()] = regulationNo;
		command[CommandBytes.byteTurnRation.ordinal()] = turnNo;
		command[CommandBytes.byteRunState.ordinal()] = runStateRunning;
		command[CommandBytes.byteTachoLimit1.ordinal()] = (byte) 0x00;
		command[CommandBytes.byteTachoLimit2.ordinal()] = (byte) 0x00;
		command[CommandBytes.byteTachoLimit3.ordinal()] = (byte) 0x00;
		command[CommandBytes.byteTachoLimit4.ordinal()] = (byte) 0x00;
	}

	public void setCoords( Float x, Float y )
	{
		this.axisX = x;
		this.axisY = y;
		this.quadrant = getQuadrant();
		this.power = getPower();
		this.strHexPower = hexStringFromInt( this.power );
		this.bytePower = powerToByteArray();

		if ( this.bytePower.length > 0 )
			command[CommandBytes.byteMotorPower.ordinal()] = this.bytePower[this.bytePower.length - 1];
		else
			command[CommandBytes.byteMotorPower.ordinal()] = (byte) 0x00;

	}

	public boolean send( OutputStream oStream )
	{
		for ( byte b : this.command )
			System.out.format( " 0x%x ", b );

		try
		{
			oStream.write( this.command, 0, this.command.length );
		}
		catch ( IOException ex )
		{
			ex.printStackTrace();

			return false;
		}

		return true;
	}

	private byte[] powerToByteArray()
	{
		return ByteBuffer.allocate( 4 ).putInt( this.power ).array();
	}

	private Quadrant getQuadrant()
	{
		if ( this.axisX > 0 && this.axisY > 0 )
		{
			return Quadrant.first;
		}
		else if ( this.axisX < 0 && this.axisY > 0 )
		{
			return Quadrant.second;
		}
		else if ( this.axisX < 0 && this.axisY < 0 )
		{
			return Quadrant.third;
		}
		else if ( this.axisX > 0 && this.axisY < 0 )
		{
			return Quadrant.fourth;
		}

		return Quadrant.invalid;
	}

	public static boolean validCoords( Float x, Float y )
	{
		return ( ( y >= -1.f && y <= 1.f ) && ( x >= -1.f && x <= 1.f ) );
	}

	private int getPower()
	{
		double powX = Math.pow( (double) this.axisX, 2 );
		double powY = Math.pow( (double) this.axisY, 2 );
		double pwr = Math.sqrt( powX + powY );

		int power = Math.abs( (int) ( pwr * 100 ) );

		int powerDelta = getPowerAddition();

		switch ( this.quadrant )
		{
			case first:
				switch ( this.motorSide )
				{
					case left:
						power += powerDelta;
						break;
					case right:
						power -= powerDelta;
						break;
					default:
						break;
				}
				break;

			case second:
				switch ( this.motorSide )
				{
					case left:
						power -= powerDelta;
						break;
					case right:
						power += powerDelta;
						break;
					default:
						break;
				}
				break;

			case third:
				switch ( this.motorSide )
				{
					case left:
						power -= powerDelta;
						power = -power;
						break;
					case right:
						power += powerDelta;
						power = -power;
						break;
					default:
						break;
				}
				break;

			case fourth:
				switch ( this.motorSide )
				{
					case left:
						power += powerDelta;
						power = -power;
						break;
					case right:
						power -= powerDelta;
						power = -power;
						break;
					default:
						break;
				}
				break;

			default:
				break;
		}

		if ( power > 100 )
			power = 100;
		else if ( power < -100 )
			power = -100;
		else if ( power > 0 && power < 25 )
			power = 25;
		else if ( power < 0 && power > -25 )
			power = -25;

		return power;
	}

	private int getPowerAddition()
	{
		return Math.abs( (int) ( this.axisX * 100 ) );
	}

	private String hexStringFromInt( int decimal )
	{
		return Integer.toHexString( decimal );
	}

	private byte getDefaultMotorPort() throws Exception
	{
		byte motorPort = MotorCmdBuilder.motorPortAll;

		switch ( this.motorSide )
		{
			case left:
				motorPort = MotorCmdBuilder.motorPortA;
				break;
			case right:
				motorPort = MotorCmdBuilder.motorPortB;
				break;
			case invalid:
				throw new Exception( "Invalid motor side." );
		}

		return motorPort;
	}
}
