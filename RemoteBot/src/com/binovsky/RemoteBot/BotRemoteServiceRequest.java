package com.binovsky.RemoteBot;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.util.Log;

public class BotRemoteServiceRequest
{
	private static final String			SERVICE				= "service";
	private static final String			SERVICE_TYPE_REMOTE	= "RestRemoteService";

	private Activity					activity			= null;
	private static RestRemoteService	service				= null;
	private static JSONParser			jsonParser			= null;
	private String						contentType			= "";

	public void setActivity( Activity act )
	{
		this.activity = act;
	}

	public Activity getActivity()
	{
		return this.activity;
	}

	public void stop()
	{
		if ( service != null )
			service.destroySession();
	}

	public String handle( String uri, String httpMethod, Properties header,
			Properties params, Properties files, Properties cntntType )
	{
		if ( service == null )
			service = new RestRemoteService();

		if ( jsonParser == null )
			jsonParser = new JSONParser();

		service.setActivity( this.activity );

		String[] uriComponents = uri.split( "/" );
		String strService = "";
		String strServiceType = "";
		String strServiceMethod = "";

		this.contentType = cntntType.getProperty( "content-type" );
		String clientId = header.getProperty( "cleint-id", null );
		String sessionId = header.getProperty( "session-id", null );

		if ( uriComponents.length > 1 )
			strService = uriComponents[1];
		if ( uriComponents.length > 2 )
			strServiceType = uriComponents[2];
		if ( uriComponents.length > 3 )
			strServiceMethod = uriComponents[3];

		if ( strService.equals( SERVICE )
				&& strServiceType.equals( SERVICE_TYPE_REMOTE ) )
		{
			// fetch pointer on service method in request
			String serviceMethod = strServiceMethod;
			Method[] methods = service.getClass().getMethods();
			Method method = null;
			for ( Method m : methods )
			{
				String strMethod = m.getName();
				if ( serviceMethod.equals( strMethod ) )
				{
					method = m;
					break;
				}
			}

			if ( method == null )
			{
				String returnString = getJsonString( "error_msg",
						"Unknown method call: '" + strServiceMethod + "'" );

				Log.w( "BotRemoteServicerequest", returnString );

				return returnString;
			}

			// parse file for 'PUT' request
			JSONObject jsonObj = null;

			if ( httpMethod.equals( "PUT" ) )
			{
				Set< Object > keysSet = null;
				keysSet = files.keySet();

				if ( keysSet.size() == 1 )
				{
					Iterator< Object > iterator = keysSet.iterator();
					String strFilePath = "";
					while ( iterator.hasNext() )
					{
						String strKey = (String) iterator.next();
						strFilePath = files.getProperty( strKey );
					}

					jsonObj = jsonParser.parse( strFilePath );
				}
				else
				{
					String notFilesForPut = getJsonString( "error_msg",
							"Error, detected 'PUT' request but no files were found!" );

					Log.w( "BotRemoteServicerequest", notFilesForPut );

					return notFilesForPut;
				}
			}

			Type returnTypeAnnot = (Type) method.getAnnotation( Type.class );
			Class returnTypeClass = (Class) returnTypeAnnot.type();
			String returnValName = returnTypeAnnot.name();
			Params annotation = (Params) method.getAnnotation( Params.class );
			@SuppressWarnings ( "rawtypes")
			Class[] parameterTypes = (Class[]) method.getParameterTypes();

			String[] names = annotation.names();
			@SuppressWarnings ( "rawtypes")
			Class[] types = annotation.types();
			Object[] args = new Object[names.length];

			// if session id was not set in headers there should be client id to
			// create a session
			if ( service.sessionId == null && clientId != null
					&& clientId.length() > 0 &&
					// httpMethod.equals( "GET" ) &&
					method.getName().equals( "connect" ) )
			{
				args[0] = clientId;
			}
			else if ( service.sessionId != null
					&& service.sessionId.equals( sessionId ) ) // session is
																// created we
																// can go on
																// service api
																// call
			{
				for ( int i = 0; i < names.length; i++ )
				{
					try
					{
						@SuppressWarnings ( "rawtypes")
						Class paramClass = types[i];

						if ( paramClass.toString().equals(
								Boolean.class.toString() ) )
						{
							args[i] = Boolean.parseBoolean( (String) jsonObj
									.get( names[i] ) );
							break;
						}
						else if ( paramClass.toString().equals(
								Float.class.toString() )
								|| paramClass.toString().equals(
										Integer.class.toString() ) )
						{
							args[i] = Float.parseFloat( (String) jsonObj.get(
									names[i] ).toString() );
						}
						else if ( paramClass.toString().equals(
								String.class.toString() ) )
						{
							args[i] = String.valueOf( jsonObj.get( names[i] )
									.toString() );
						}
						else
						{
							args[i] = paramClass.cast( jsonObj.get( names[i] )
									.toString() );
						}
					}
					catch ( JSONException e )
					{
						e.printStackTrace();
					}

				}
			}

			if ( names.length != args.length )
			{
				String strError = getJsonString( "error_msg",
						"Error, could not parse input parameters!" );

				Log.w( "BotRemoteServicerequest", "Return: " + strError );

				return strError;
			}

			Object returnVal = null;

			try
			{
				Log.w( "BotRemoteServicerequest",
						"Invoking: " + method.getName() );
				returnVal = method.invoke( service, args );
			}
			catch ( IllegalAccessException e1 )
			{
				e1.printStackTrace();
			}
			catch ( IllegalArgumentException e2 )
			{
				e2.printStackTrace();
			}
			catch ( InvocationTargetException e3 )
			{
				e3.printStackTrace();
			}

			if ( returnVal == null )
			{
				String strError = getJsonString( "error_msg",
						"Return value from API was null!" );

				Log.w( "BotRemoteServicerequest", "Return: " + strError );

				return strError;
			}
			
			Log.w( "BotRemoteServicerequest", returnVal.getClass().toString() );

			if ( returnVal.getClass().toString()
					.equals( returnTypeClass.toString() ) )
			{
				JSONObject returnJson = new JSONObject();

				try
				{
					if ( returnTypeClass.toString().equals(
							String.class.toString() ) )
						returnJson.put( returnValName, returnVal.toString() );
					else if ( returnTypeClass.toString().equals(
							Boolean.class.toString() ) )
						returnJson.put( returnValName,
								( (Boolean) returnVal ).booleanValue() );
					else if ( returnTypeClass.toString().equals(
							Integer.class.toString() ) )
						returnJson.put( returnValName,
								( (Integer) returnVal ).intValue() );
					else if ( returnTypeClass.toString().equals(
							Float.class.toString() ) )
						returnJson.put( returnValName,
								( (Float) returnVal ).floatValue() );
					else if ( returnTypeClass.toString().equals(
							HashMap.class.toString() ) )
						returnJson.put( returnValName, new JSONObject(
								(HashMap< String, Object >) returnVal ) );

					cntntType.clear();
					cntntType.put( "content-type", "application/json" );

					Log.w( "BotRemoteServicerequest",
							"Return: " + returnJson.toString() );

					return returnJson.toString();
				}
				catch ( JSONException e )
				{
					e.printStackTrace();

					String strError = getJsonString( "error_msg",
							"Type cast error while generating response message!" );

					Log.w( "BotRemoteServicerequest", "Return: " + strError );

					return strError;
				}

			}
		}

		Log.w( "BotRemoteServicerequest",
				"Not a service call, returning just 'index.html' page." );

		return service.index();
	}

	private String getJsonString( String key, String message )
	{
		JSONObject returnJson = new JSONObject();

		try
		{
			returnJson.put( key, message );
		}
		catch ( JSONException e )
		{
			e.printStackTrace();
		}

		return returnJson.toString();
	}
}
