package com.binovsky.RemoteBot;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import android.app.Activity;
import android.os.Handler;

public class RemoteBotHTTPD extends NanoHTTPD
{
	public static final int					PORT				= 8080;
	private static final String				SERVICE				= "service";
	private static final String				SERVICE_TYPE_REMOTE	= "RestRemoteService";

	private Handler							handler				= new Handler();
	private Activity						activity			= null;
	private static BotRemoteServiceRequest	request				= null;

	public RemoteBotHTTPD( File wwwRoot ) throws IOException
	{
		super( PORT, wwwRoot );
	}

	public void setActivity( Activity act )
	{
		this.activity = act;
	}

	public Activity getActivity()
	{
		return this.activity;
	}

	@Override
	public Response serve( String uri, String method, Properties header,
			Properties params, Properties files, Properties contentType )
	{
		final StringBuilder buf = new StringBuilder();
		for ( Entry< Object, Object > kv : header.entrySet() )
		{
			buf.append( kv.getKey() + " : " + kv.getValue() + "\n" );
		}

		handler.post( new Runnable()
		{
			@Override
			public void run()
			{
				if ( activity.getClass().equals( MainActivity.class ) )
				{
					MainActivity mainActivity = (MainActivity) activity;
					buf.append( mainActivity.console.getText() );
					mainActivity.console.setText( buf );
				}
			}
		} );

		request = new BotRemoteServiceRequest();
		request.setActivity( this.activity );
		String strResponse = request.handle( uri, method, header, params,
				files, contentType );

		return new NanoHTTPD.Response( HTTP_OK,
				contentType.getProperty( "Content-Type" ), strResponse );
	}

	@Override
	public void stop()
	{
		super.stop();

		if ( request != null )
			request.stop();

	}
}
