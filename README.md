DP
==
/*
******************************************************************

Copyright (c) 2013, Michal Bínovský
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are not permitted.

******************************************************************
*/


/* 
   **************************************************************
   ****************************** EN ****************************
   ************************************************************** 
*/
Diploma Thesis Source Code

- source code for theme of diploma thesis: "Controling and monitoring of robot by web services"


/* 
   **************************************************************
   ****************************** SVK ***************************
   ************************************************************** 
*/
Zdrojový kód k Diplopmovému projektu

- zdrojový kód na diplomový projekt s témou: "Ovládanie a monitorovanie robota pomocou webových služieb"




